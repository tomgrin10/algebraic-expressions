﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algebra
{
    public class AlgVariable : AlgExpression
    {
        char name { get; }

        public AlgVariable(char name)
        {
            if ((name >= 'a' && name <= 'z') || (name >= 'A' && name <= 'Z'))
            {
                this.name = name;
            }
            else
                throw new ArgumentException();
        }

        public override AlgExpression Simplify() => this;

        public override string ToString()
        {
            return name.ToString();
        }
    }
}
