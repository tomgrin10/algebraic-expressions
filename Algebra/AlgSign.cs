﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algebra
{
    abstract class AlgSign : AlgExpression
    {
        public char signChar { get; }

        public AlgExpression left { get; set; }
        public AlgExpression right { get; set; }

        public AlgSign(char signChar, string leftStr, string rightStr)
        {
            this.signChar = signChar;
            left = MakeAlgExpression(leftStr);
            right = MakeAlgExpression(rightStr);
        }
    }
}