﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algebra
{
    public abstract class AlgExpression
    {
        protected AlgExpression() { }

        public static AlgExpression MakeAlgExpression(string str)
        {
            str = str.RemoveWhitespace();
            if (str.Length == 0)
                throw new ArgumentException();

            foreach (char ch in str)
            {
                if (ch == '+')
                {
                    string[] strArr = str.Split('+');
                    return new AlgPlus(strArr[0], strArr[1]);
                }
            }

            if (str[0].IsLetter())
                return new AlgVariable(str[0]);
            else
                return new AlgNumber(double.Parse(str));
        }

        abstract public AlgExpression Simplify();
        abstract public override string ToString();
    }
}