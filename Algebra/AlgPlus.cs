﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algebra
{
    class AlgPlus : AlgSign
    {
        public AlgPlus(string leftStr, string rightStr) : base('+', leftStr, rightStr) { }

        public override AlgExpression Simplify()
        {
            left.Simplify();
            right.Simplify();

            return this;
        }

        public override string ToString()
        {
            return left.ToString() + base.signChar + right.ToString();
        }
    }
}
