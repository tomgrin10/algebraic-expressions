﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algebra
{
    class AlgNumber : AlgExpression
    {
        public double value { get; set; }

        public AlgNumber(double value)
        {
            this.value = value;
        }

        public override AlgExpression Simplify() => this;

        public override string ToString()
        {
            return value.ToString();
        }
    }
}
