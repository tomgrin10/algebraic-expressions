﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Algebra
{
    static class Utils
    {
        public static bool IsLetter(this char ch)
        {
            return (ch >= 'a' && ch <= 'z') || (ch >= 'A' && ch <= 'Z');
        }

        public static string RemoveWhitespace(this string str)
        {
            string newStr = "";
            foreach (char ch in str)
            {
                if (ch != ' ' && ch != '\t')
                    newStr += ch;
            }

            return newStr;
        }
    }
}
